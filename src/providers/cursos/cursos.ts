import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

/*
  Generated class for the CursosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CursosProvider {

  constructor(public http: HttpClient) {
    console.log('Hello CursosProvider Provider');
  }

  listado(apikey: string)
  {
    let url = 'https://api.sebastian.cl/academia/api/v1/courses/subjects'
    let httpOptions = {
      headers: new HttpHeaders({ 'X-API-KEY': apikey})
    };
      return this.http.get(url, httpOptions)
  }


  añoscurso(apikey:string, codigo: string)
  {
    let url = 'https://api.sebastian.cl/academia/api/v1/rankings/years/'+codigo;
    let httpOptions = {
      headers: new HttpHeaders({ 'X-API-KEY': apikey})
    };
    return this.http.get(url, httpOptions)
  }

  añosgeneral(apikey:string)
  {
    let url = 'https://api.sebastian.cl/academia/api/v1/rankings/years';
    let httpOptions = {
      headers: new HttpHeaders({ 'X-API-KEY': apikey})
    };
    return this.http.get(url, httpOptions)
  }
  
  listadoE(apikey: string, rut: string)
  {
    let url = 'https://api.sebastian.cl/academia/api/v1/courses/students/'+rut;
    let httpOptions = {
      headers: new HttpHeaders({ 'X-API-KEY': apikey})
    };
      return this.http.get(url, httpOptions)
  }

}




  
